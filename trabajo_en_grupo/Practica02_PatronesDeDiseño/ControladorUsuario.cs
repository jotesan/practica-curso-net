﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Practica02_PatronesDeDiseño
{
    public class ControladorUsuario
    {
        IModeloGenerico<Usuario> modelo;

        public ControladorUsuario()
        {
            this.modelo = ModeloUsuario.Instancia;
        }

        public bool Crear(Usuario nuevoOb)
        {
            return modelo.Crear(nuevoOb);
        }

        public IList<Usuario> LeerTodos()
        {
            return modelo.LeerTodos();
        }

       public Usuario LeerUno<TBusqueda>(TBusqueda dato)
        {
            return modelo.LeerUno(dato);
        }

        public bool Eliminar<TBusqueda>(TBusqueda dato)
        {
            return modelo.Eliminar(dato);
        }

        public Usuario Modificar<TBusqueda, TCampo>(TBusqueda dato, string nombre, TCampo campo)
        {
            return modelo.Modificar(dato,nombre,campo);
        }
    }
}
