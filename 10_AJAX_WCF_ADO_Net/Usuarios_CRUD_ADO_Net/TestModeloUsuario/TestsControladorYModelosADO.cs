using NUnit.Framework;
using ModeloUsuarios;
using System;
using System.Reflection;
using System.Collections.Generic;

namespace TestModeloUsuario
{
    public class TestsControladorYModelosADO
    {
        ModeloUsuario modelo;
        ModuloPersistenciaADO moduloPersistencia;
        Usuario usu1;
        List<int> id_aux = new List<int>();



        [SetUp]
        public void Setup()
        {
            moduloPersistencia = new ModuloPersistenciaADO(Entorno.Desarrollo);
            modelo = ModeloUsuario.Instancia;
        }

        [TearDown]
        public void AlTerminarTest()
        {
            EliminarUsuariosValidos();
        }
        
        void CrearUsuariosValidos()
        {
            List<Usuario> lista_usuarios_validos = new List<Usuario>();
            lista_usuarios_validos.Add(new Usuario("valido1",10,1.7f,true));
            lista_usuarios_validos.Add(new Usuario("valido2",20,1.8f, true));
            lista_usuarios_validos.Add(new Usuario("valido3", 30, 1.9f, true));

            foreach (Usuario usuario in lista_usuarios_validos)
            {
                moduloPersistencia.Crear(usuario);
            }
            
            List<Usuario> lista_todos_usuarios = moduloPersistencia.Leer();

            foreach (Usuario valido in lista_usuarios_validos)
            {
                foreach (Usuario u in lista_todos_usuarios)
                {
                    if (valido.Equals(u)) {
                        id_aux.Add((int) u.Id);
                        break;
                    }
                }
            }
        }

        List<Usuario> LeerUsuarioValidos()
        {
            List<Usuario> validos = new List<Usuario>();
            List<Usuario> todos = moduloPersistencia.Leer();

            foreach (int id  in id_aux)
            {
                foreach (Usuario u in todos)
                {
                    if (u.Id == id)
                    {
                        validos.Add(u);
                        break;
                    }
                }
            }

            return validos;
        }

        void EliminarUsuariosValidos()
        {
            foreach (int id in id_aux)
            {
                moduloPersistencia.Eliminar(id);
            }
            id_aux.Clear();
        }

        [Test]
        public void TestCrearValidos()
        {
            int n_ant = moduloPersistencia.Leer().Count;
            CrearUsuariosValidos();

            List<Usuario> todos = moduloPersistencia.Leer();

            Assert.AreNotEqual(n_ant, todos.Count, "No se han creado tres usuarios");

            foreach (int id in id_aux)
            {
                bool existe_usuario_con_id = false;
                foreach (Usuario u in todos)
                {
                    if (u.Id == id)
                    {
                        existe_usuario_con_id = true;
                        break;
                    }
                }
                Assert.IsTrue(existe_usuario_con_id, "No se ha creado el usuario con id {0}", id);
            }
        }

        [Test]
        public void TestModificar()
        {

            CrearUsuariosValidos();
            List<Usuario> todos_antes = moduloPersistencia.Leer();
            List<Usuario> validos_antes = LeerUsuarioValidos();

            List<Usuario> modificados = new List<Usuario>();
            int i = 1;
            foreach (int id in id_aux)
            {
                modificados.Add(new Usuario("modificado"+i.ToString(),i*10,1.8f, true));
                modificados[i - 1].Id = id;
                i++;
            }

            foreach (Usuario  usu in modificados)
            {
                moduloPersistencia.Modificar(usu);
            }

            List<Usuario> todos_ahora = moduloPersistencia.Leer();
            List<Usuario> validos_ahora = LeerUsuarioValidos();

            Assert.AreEqual(todos_ahora.Count, todos_antes.Count, "No hay el mismo numero de usuarios totales");
            Assert.AreEqual(validos_antes.Count, validos_ahora.Count, "No hay el mismo numero de usuarios validos");

            for (i = 0; i < validos_ahora.Count; i++)
            {
                Console.WriteLine(validos_ahora[i].ToString());
                Console.WriteLine(validos_antes[i].ToString());
                Assert.IsFalse(validos_ahora[i].Equals(validos_antes[i]), "El usuario de id {0},{1} no ha sido modificado", validos_antes[i].Id,validos_ahora[i].Id);
            }
        }
       

        [Test]
        public void TestEliminar()
        {
            List<Usuario> todos_antes = moduloPersistencia.Leer();
            CrearUsuariosValidos();

            List<Usuario> todos_con_validos = moduloPersistencia.Leer();
            List<Usuario> validos = LeerUsuarioValidos();

            EliminarUsuariosValidos();
            List<Usuario> todos_despues = moduloPersistencia.Leer();

            Assert.AreEqual(todos_antes.Count,todos_despues.Count,"No concuerda el numero de usuarios antes y despues de Eliminar");
        }

        [Test]
        public void TestLeerUno()
        {
            CrearUsuariosValidos();
            List<Usuario> validos = LeerUsuarioValidos();

            Usuario usuario_leido = moduloPersistencia.LeerUno((int) validos[0].Id);

            Assert.AreEqual(validos[0].Id, usuario_leido.Id, "Los id no coinciden");
            Assert.IsTrue(validos[0].Equals(usuario_leido), "El usuario leido y el valido no concuerda");
        }
        
        [Test]
        public void TestLeerTodos()
        {
            List<Usuario> todos_antes = moduloPersistencia.Leer();
            CrearUsuariosValidos();
            List<Usuario> todos_despues = moduloPersistencia.Leer();
            Assert.Greater(todos_despues.Count, todos_antes.Count, "No se han leido todos los usuarios");
        }
        
       [Test]

        public void TestCrearInvalidos()
        {

            var ex1 = Assert.Throws<ArgumentNullException>(() => new Usuario("",10,1.5f, true));
            Assert.IsTrue(ex1.Message.Contains("(codigo:30)"));

            var ex2 = Assert.Throws<ArgumentOutOfRangeException>(() => new Usuario("Invalido2",-5,1.5f, true));
            Assert.IsTrue(ex2.Message.Contains("(codigo:31)"));

            var ex3 = Assert.Throws<ArgumentOutOfRangeException>(() => new Usuario("Invalido3",150,1.5f, true));
            Assert.IsTrue(ex3.Message.Contains("(codigo:31)"));

            var ex4 = Assert.Throws<ArgumentOutOfRangeException>(() => new Usuario("Invalido4",15,-3f, true));
            Assert.IsTrue(ex4.Message.Contains("(codigo:32)"));

            var ex5 = Assert.Throws<ArgumentOutOfRangeException>(() => new Usuario("Invalido5",15,3.5f, true));
            Assert.IsTrue(ex5.Message.Contains("(codigo:32)"));

        }
    }

}