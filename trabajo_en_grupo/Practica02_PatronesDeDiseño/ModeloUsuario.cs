﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.Json;

namespace Practica02_PatronesDeDiseño
{
    public class ModeloUsuario : IModeloGenerico<Usuario>
    {

        List<Usuario> lista;

        public Action OnChange;

        public Action<Usuario> CallOnCreate;
        public Action<Usuario> CallOnDelete;
        public Action<Usuario> CallOnModified;

        private static ModeloUsuario instancia = null;

        private ModeloUsuario()
        {
            this.lista = new List<Usuario>();
            OnChange = () => { return; };
            CallOnCreate = (u) => { return; };
            CallOnDelete = (u) => { return; };
            CallOnModified = (u) => { return; };
        }

        public static ModeloUsuario Instancia
        {
            get
            {
                if (instancia == null)
                {
                    instancia = new ModeloUsuario();
                }
                return instancia;
            }
        }

        public bool Crear(Usuario nuevoOb)
        {
            lista.Add(nuevoOb);
            OnChange();
            CallOnCreate(nuevoOb);
            return true;
        }

        public bool Eliminar<TBusqueda>(TBusqueda dato)
        {
            Usuario user = GetUsuarioFromType<TBusqueda>(dato);

            if (user == null) { return false; }
            else
            {
                lista.Remove(user);
                OnChange();
                CallOnDelete(user);
                return true;
            }
        }

        public IList<Usuario> LeerTodos()
        {
            return lista;
        }

        public Usuario LeerUno<TBusqueda>(TBusqueda dato)
        {
            return GetUsuarioFromType(dato);
        }

        public Usuario Modificar<TBusqueda,TCampo>(TBusqueda dato, string nombre, TCampo campo)
        {
            Usuario user = GetUsuarioFromType(dato);

            if (user == null) { return user; }
            else
            {
                Usuario userCambiado = CambiarCampoNombre(user, nombre, campo);
                OnChange();
                CallOnModified(user);
                return userCambiado;
            }
        }

        private Usuario CambiarCampoNombre<TCampo>(Usuario user,string nombreClase, TCampo campo)
        {
            if (nombreClase == "edad")
            {
                int edad = (int)(object)campo;
                user.Edad = edad;
            }
            else if (nombreClase == "nombre")
            {
                string nombre = (string)(object)campo;
                user.Nombre = nombre;
            }
            else if (nombreClase == "altura")
            {
                float altura = (float)(object)campo;
                user.Altura = altura;
            }

            return user;
        }

        private Usuario CambiarCampoTipo<TCampo>(Usuario user, TCampo campo)
        {
            if (typeof(TCampo) == typeof(int))
            {
                int edad = (int)(object)campo;
                user.Edad = edad;
            }else if (typeof(TCampo) == typeof(string))
            {
                string nombre = (string)(object)campo;
                user.Nombre = nombre;   
            }
            else if (typeof(TCampo) == typeof(float))
            {
                float altura = (float)(object)campo;
                user.Altura = altura;
            }

            return user;
        }

        private Usuario GetUsuarioFromType<TBusqueda>(TBusqueda dato)
        {
            Usuario resultado = null;
            if (typeof(TBusqueda) == typeof(int))
            {
                int id = (int)(object)dato;

                if ((id >= lista.Count) || (id <0))
                {
                    Console.Error.WriteLine("ERROR: El indice introducido es mayor que el numero de usuarios");
                }
                else
                {
                    resultado = lista[id];
                }

            }
            else if (typeof(TBusqueda) == typeof(string))
            {
                foreach (Usuario user in lista)
                {
                    if (user.Nombre.ToUpper() == (string)(object)dato.ToString().ToUpper())
                    {
                        resultado = user;
                        break;
                    }
                }
                if (resultado == null)
                {
                    Console.Error.WriteLine("ERROR: No hay usuarios con este nombre");
                }
            }
            else if (typeof(TBusqueda) == typeof(Usuario))
            {
                resultado = (Usuario)(object)dato;
            }
            else
            {
                Console.Error.WriteLine("ERROR: Tipo de dato no valido");
            }
            return resultado;
        }
    }
}
