﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Practica02_PatronesDeDiseño
{
    public static class VistaAzul
    {
        public static void AsignarFunciones()
        {
            ModeloUsuario.Instancia.CallOnCreate = (user) => VistaAzul.MostrarCambios(user, "Creado");
            ModeloUsuario.Instancia.CallOnDelete = (user) => VistaAzul.MostrarCambios(user, "Borrado");
            ModeloUsuario.Instancia.CallOnModified = (user) => VistaAzul.MostrarCambios(user, "Modificado");
        }
        public static void MostrarCambios(Usuario user, string accion)
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("El usuario {0} ha sido {1}", user.Nombre, accion);
            Console.ForegroundColor = ConsoleColor.White;
        }
    }
}
