﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Practica01_GestionDeUsuario
{
    public class GestionUsuario
    {

        List<Usuario> listaUsuario;

        public GestionUsuario()
        {
            this.listaUsuario = new List<Usuario>();
        }

        public void MostrarUno<Tipo>(Tipo dato)
        {
            Usuario resultado = getUsuarioFromType(dato);
            if (resultado != null)
            {
                resultado.MostrarDatosConsola();
            }
        }

        public void MostrarTodos()
        {

            if (listaUsuario.Count == 0)
            {
                Console.WriteLine("No hay ningun usuario");
            }
            else
            {
                foreach (Usuario user in listaUsuario)
                {
                    user.MostrarDatosConsola();
                }
            }

        }

        public void EliminarUno<Tipo>(Tipo dato)
        {
            Usuario resultado = getUsuarioFromType(dato);
            if (resultado != null)
            {
                listaUsuario.Remove(resultado);
            }
        }

        public void CrearUno()
        {
            Usuario usuario = new Usuario();
            usuario.PedirDatosConsola();
            listaUsuario.Add(usuario);
        }

        public void ModificarUno<Tipo>(Tipo dato)
        {
            Usuario resultado = getUsuarioFromType(dato);
            if (resultado != null)
            {
                resultado.PedirDatosConsola();
            }
            
        }

        private Usuario getUsuarioFromType<Tipo>(Tipo dato)
        {
            Usuario resultado = null;
            if (typeof(Tipo) == typeof(int))
            {
                int id = (int)(object)dato;

                if (id>= listaUsuario.Count)
                {
                    Console.Error.WriteLine("ERROR: El indice introducido es mayor que el numero de usuarios");
                }
                else
                {
                    resultado = listaUsuario[id];
                }
                
            }
            else if (typeof(Tipo) == typeof(string))
            {
                foreach (Usuario user in listaUsuario)
                {
                    if (user.Nombre == (string)(object)dato)
                    {
                        resultado = user;
                        break;
                    }
                }
                if (resultado == null)
                {
                    Console.Error.WriteLine("ERROR: No hay usuarios con este nombre");
                }
            }
            else if (typeof(Tipo) == typeof(Usuario))
            {
                resultado = (Usuario)(object)dato;
            }
            else
            {
                Console.Error.WriteLine("ERROR: Tipo de dato no valido");
            }
            return resultado;
        }

    }
}
