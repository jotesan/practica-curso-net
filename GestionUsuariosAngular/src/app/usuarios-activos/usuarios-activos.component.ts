import { Component, OnInit } from '@angular/core';
import { ServicioUsuariosService } from '../servicio-usuarios.service';
import { Usuario } from '../usuario';

@Component({
  selector: 'app-usuarios-activos',
  templateUrl: './usuarios-activos.component.html',
  styleUrls: ['./usuarios-activos.component.css']
})
export class UsuariosActivosComponent implements OnInit {

  constructor(public servicioUsuarios: ServicioUsuariosService) {
  }

  ngOnInit(): void {
  }

}
