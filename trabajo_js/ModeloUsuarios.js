class Modelo{
    
    constructor(){

        let usuariosGuardados = JSON.parse(window.localStorage.getItem("usuarios"));
        if (usuariosGuardados === null) {
            usuariosGuardados = [];
        }
        this.usuarios = usuariosGuardados;
        this.onChange = null;
    }

    crearUsuario(usuario){
        this.usuarios.push(usuario);
        this.guardarUsuarios();
        this.onChange(this.usuarios);
    }

    eliminarUsuario(index){
        this.usuarios.splice(index,1);
        this.guardarUsuarios();
        this.onChange(this.usuarios);
    }

    leerUsuario(index){
        return this.usuarios[index];
    }

    leerTodosUsuarios(){
        return this.usuarios.slice();
    }

    modificarUsuario(index,usuario){
        this.usuarios[index] = usuario;
        this.guardarUsuarios();
        this.onChange(this.usuarios);
    }

    guardarUsuarios(){
        window.localStorage.clear();
        window.localStorage.setItem("usuarios", JSON.stringify(this.usuarios));
    }

    borrarStorage(){
        window.localStorage.clear();
        this.usuarios = [];
        this.onChange(this.usuarios);
    }

}


