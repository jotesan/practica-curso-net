﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Practica02_PatronesDeDiseño
{
    class VistaUsuario : IVistaGenerico
    {
        ControladorUsuario controlador;

        public VistaUsuario(ControladorUsuario controlador)
        {
            this.controlador = controlador;
        }

        public void Menu()
        {
            int opcion = 0;
            do
            {
                Console.WriteLine("MENU: ( 0 -Salir)");
                Console.WriteLine("1 - Alta usuario");
                Console.WriteLine("2 - Mostrar todos los usuarios");
                Console.WriteLine("3 - Mostrar un usuario");
                Console.WriteLine("4 - Eliminar un usuario");
                Console.WriteLine("5 - Modificar un usuario");
                string str_op = Console.ReadLine();
                Console.Clear();
                if (int.TryParse(str_op, out opcion))
                {
                    switch (opcion)
                    {
                        case 1:
                            Alta();
                            break;
                        case 2:
                            MostrarTodos();
                            break;
                        case 3:
                            MostrarUno();
                            break;
                        case 4:
                            Baja();
                            break;
                        case 5:
                            Modificar();
                            break;
                        default:
                            if (opcion != 0) Console.WriteLine("Introduzca opción válida");
                            break;
                    }
                }
                else
                {
                    opcion = -1;
                    Console.WriteLine("Escribe bien el número");
                }
                Console.ReadKey();
                Console.Clear();
            } while (opcion != 0);
        }

        public void Alta()
        {
            string nombre;
            int edad;
            float altura;

            Console.WriteLine("Alta Usuario: ");

            UIConsole.PedirString("nombre", out nombre);
            UIConsole.PedirNum("edad", out edad);
            UIConsole.PedirNum("altura", out altura);
            bool creado = controlador.Crear(new Usuario(nombre, edad, altura));
            if (creado)
            {
                Console.WriteLine("Se dio de alta el usuario " + nombre);
            }
            else
            {
                Console.WriteLine("No se dio de alta el usuario " + nombre);
            }
        }

        public void Baja()
        {
            Console.WriteLine("Introduce el nombre o la posición del usuario: ");

            bool eliminado;
            int entero;
            string str = Console.ReadLine();

            eliminado = controlador.Eliminar(str);

            if (!eliminado)
            {
                if (int.TryParse(str, out entero)){

                    eliminado = controlador.Eliminar(entero);

                }
            }

            if (eliminado)
            {
                Console.WriteLine("Usuario eliminado correctamente");
            }
            else
            {
                Console.WriteLine("No se pudo eliminar el usuario");
            }
        }

        public void Modificar()
        {
            Usuario user = null;
            string str;
            int id;
            string nombre;
            string edad;
            string altura;
            int entero;

            Console.WriteLine("Introduce el nombre o la posición del usuario: ");
            str = Console.ReadLine();

            if (int.TryParse(str, out entero))
            {
                user = controlador.LeerUno(entero);
            }
            else
            {
                user = controlador.LeerUno(str);
            }

            if (user == null)
            {
                Console.WriteLine("Usuario no encontrado");
                return;
            }
            Console.WriteLine("Nuevo nombre: ");
            nombre = Console.ReadLine();

            UIConsole.StringValidoInt("Edad", out edad);

            UIConsole.StringValidoFloat("Altura", out altura);

            if (int.TryParse(str, out id))
            {
                if (edad != "") { user = controlador.Modificar(id, "edad", int.Parse(edad)); }
                if (altura != "") { user = controlador.Modificar(id, "altura", float.Parse(altura)); }
                if (nombre != "") { user = controlador.Modificar(id, "nombre", nombre); }
            }
            else
            {
                if (edad != "") { user = controlador.Modificar(str, "edad", int.Parse(edad)); }
                if (altura != "") { user = controlador.Modificar(str, "altura", float.Parse(altura)); }
                if (nombre != "") { user = controlador.Modificar(str, "nombre", nombre); }
            }

            if (user != null)
            {
                Console.WriteLine("Usuario " + str + " modificado correctamente");
            }
            else
            {
                Console.WriteLine("Usuario no encontrado");
            }
        }

        public void MostrarTodos()
        {
            IList<Usuario> usuarios = controlador.LeerTodos();
            foreach (Usuario usuario in usuarios)
            {
                Console.WriteLine("Usaurio: " + usuario.ToString());
            }
        }

        public void MostrarUno()
        {
            Console.WriteLine("Introduce el nombre o la posición del usuario: ");
            Usuario user;
            int entero;
            string str = Console.ReadLine();
            if (int.TryParse(str, out entero))
            {
                user = controlador.LeerUno(entero);
            }
            else
            {
                user = controlador.LeerUno(str);
            }

            if (user != null)
            {
                Console.WriteLine("Usuario: " + user.ToString());
            }
            else
            {
                Console.WriteLine("Usuario no encontrado");
            }

        }
    }
}
