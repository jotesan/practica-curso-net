﻿using System;

namespace Practica02_PatronesDeDiseño
{
    class Program
    {
        ControladorUsuario cu;
        VistaUsuario vu;

        void InsertarUsuarios()
        {
            cu.Crear(new Usuario("Pepe", 30, 1.5f));
            cu.Crear(new Usuario("Ana", 24, 1.68f));
            cu.Crear(new Usuario("Jose", 15, 1.72f));
            cu.Crear(new Usuario("Paula", 50, 1.8f));
        }

        Program()
        {
            cu = new ControladorUsuario();
            vu = new VistaUsuario(cu);

            Persistencia.GetUsers("UsuariosJson.json");
        }

        static void Main(string[] args)
        {
            Program program = new Program();
            Persistencia.AsignarAccion("UsuariosJson.json");
            VistaAzul.AsignarFunciones();
            //program.InsertarUsuarios();
            program.vu.Menu();

            //ModeloUsuario.Instancia.TransformarJSON();



        }
    }
}
