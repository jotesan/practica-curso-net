﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace ModeloUsuarios
{
    
    [ApiController]
    [Route("api/[controller]")]
    public class UsuariosController : ControllerBase
    {
        ModeloUsuario modeloUsuario;

        private readonly ILogger<UsuariosController> _logger;

        public UsuariosController(ILogger<UsuariosController> logger)
        {
            this.modeloUsuario = ModeloUsuario.Instancia;
            _logger = logger;
        }

        [HttpPost("/crear")]
        public Usuario Crear([FromBody] Usuario userJSON)
        {

            modeloUsuario.Crear(userJSON);

            IList<Usuario> lista = this.LeerTodos();

            int id = 0;

            foreach (Usuario u in lista)
            {
                id = (int) u.Id > id ? (int) u.Id : id;
            }

            return LeerUno(id);
        }

        [HttpGet("/leerTodos")]
        public IList<Usuario> LeerTodos()
        {
            return modeloUsuario.LeerTodos();
        }

        [HttpGet("/leerUno/{usuarioId}")]
        public Usuario LeerUno(int usuarioId)
        {
            Usuario u =  modeloUsuario.LeerUno(usuarioId);
            if (u is null) return new Usuario("El usuario que busca no existe",1,1f,true);
            else return u;
        }

        [HttpDelete("/eliminar/{usuarioId}")]
        public bool Eliminar(int usuarioId)
        {
            return modeloUsuario.Eliminar(usuarioId);
        }

        [HttpPut("/modificar")]
        public Usuario Modificar(Usuario usuario)
        {
            modeloUsuario.Modificar(usuario);

            return LeerUno((int) usuario.Id);
        }
    }
}
