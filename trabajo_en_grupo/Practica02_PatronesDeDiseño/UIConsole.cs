﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Practica02_PatronesDeDiseño
{
    public class UIConsole
    {

        public static void PedirString(string nombreDato, out string varDato)
        {
            int aux;
            Console.WriteLine("Introduce el campo " + nombreDato + ": ");
            varDato = Console.ReadLine();

            bool isValueInt = int.TryParse(varDato, out aux);
            
            while (isValueInt)
            {
                Console.WriteLine("El nombre no puede ser un numero");
                Console.WriteLine("Introduce el campo " + nombreDato + ": ");
                varDato = Console.ReadLine();
                isValueInt = (int.TryParse(varDato, out aux));
            }
        }

        public static void PedirNum<Tipo>(string nombreDato, out Tipo varDato)
        {
            Console.WriteLine("Introduce el campo " + nombreDato + ": ");
            string str = Console.ReadLine();
            bool isValueOK;

            if (typeof(Tipo) == typeof(int))
            {
                int entero;
                isValueOK = int.TryParse(str, out entero);
                varDato = (Tipo)(object)entero;
            }
            else if (typeof(Tipo) == typeof(float))
            {
                float flotante;
                isValueOK = float.TryParse(str, out flotante);
                varDato = (Tipo)(object)flotante;
            }
            else if (typeof(Tipo) == typeof(double))
            {
                double doble;
                isValueOK = double.TryParse(str, out doble);
                varDato = (Tipo)(object)doble;
            }
            else
            {
                varDato = (Tipo)(object)0;
                isValueOK = true;
                Console.Error.WriteLine("ERROR: tipo de dato no valido");
            }

            while (!isValueOK)
            {
                Console.WriteLine("por favor, Introduzca un numero.");
                str = Console.ReadLine();
                if (typeof(Tipo) == typeof(int))
                {
                    int entero;
                    isValueOK = int.TryParse(str, out entero);
                    varDato = (Tipo)(object)entero;
                }
                else if (typeof(Tipo) == typeof(float))
                {
                    float flotante;
                    isValueOK = float.TryParse(str, out flotante);
                    varDato = (Tipo)(object)flotante;
                }
                else if (typeof(Tipo) == typeof(double))
                {
                    double doble;
                    isValueOK = double.TryParse(str, out doble);
                    varDato = (Tipo)(object)doble;
                }

            }
        }

        public static void StringValidoInt(string nombreDato, out string str)
        {
            Console.WriteLine("Nuevo " + nombreDato);
            str = Console.ReadLine();
            int varDato;

            while (str != "" && !int.TryParse(str,out varDato))
            {
                Console.WriteLine("Entrada invalida, introduzca un entero o cadena vacia");
                str = Console.ReadLine();
            }
        }

        public static void StringValidoFloat(string nombreDato, out string str)
        {
            Console.WriteLine("Nuevo " + nombreDato);
            str = Console.ReadLine();
            float varDato;

            while (str != "" && !float.TryParse(str,out varDato))
            {
                Console.WriteLine("Entrada invalida, introduzca un float o cadena vacia");
                str = Console.ReadLine();
            }
        }
    }
}
