import { Component, OnInit } from '@angular/core';
import { ServicioUsuariosService } from '../servicio-usuarios.service';
import { Usuario } from '../usuario';

@Component({
  selector: 'app-usuarios-adultos',
  templateUrl: './usuarios-adultos.component.html',
  styleUrls: ['./usuarios-adultos.component.css']
})
export class UsuariosAdultosComponent implements OnInit {

  constructor(private userService : ServicioUsuariosService) { }

  ngOnInit(): void {
  }

  public todosUsuarios() : Array<Usuario>{

    return this.userService.arrayUsuarios();

  }

  public usuariosAdultos() : Array<Usuario>{
    
    return this.userService.usuariosMayores();

  }

  public usuariosMenores() : Array<Usuario>{
    
    return this.userService.usuariosMenores();

  }

}
