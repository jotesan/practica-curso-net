class Controlador {

    constructor(vista) {
        this.modelo = new Modelo();
        this.modelo.onChange = (usuarios) => {
            vista.actualizarPagina(usuarios);
        }
    }

    crearUsuario(usuario){
       this.modelo.crearUsuario(usuario);
    }

    eliminarUsuario(index){
        this.modelo.eliminarUsuario(index);
    }

    leerUsuario(index){
        return this.modelo.leerUsuario(index);
    }

    leerTodosUsuarios(){
        return this.modelo.leerTodosUsuarios();
    }

    modificarUsuario(index,usuario){
        this.modelo.modificarUsuario(index,usuario);
    }

    borrarStorage(){
        this.modelo.borrarStorage();
    }

}

