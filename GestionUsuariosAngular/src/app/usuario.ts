import { NumberSymbol } from "@angular/common";

export class Usuario {

    public id : number;
    public nombre : string;
    public edad : number;
    public altura : number;
    public activo : boolean;

    constructor(id : number, nombre: string, edad: number, altura : number, activo: boolean){
        this.id = id;
        this.nombre = nombre; 
        this.edad = edad;
        this.altura = altura;
        this.activo = activo;
    }

}
