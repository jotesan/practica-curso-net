﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Text.Json;

using System.Text.Json.Serialization;

namespace ModeloUsuarios
{
    public class ModuloPersistenciaADO
    {
		string CONEX_BD;
        static Entorno entorno = Entorno.Desarrollo;
        ModeloUsuario modelo;

        List<Usuario> listaUsuarios = new List<Usuario>();


        public ModuloPersistenciaADO(Entorno entorno)
        {
            ModuloPersistenciaADO.entorno = entorno;


            if (entorno == Entorno.Desarrollo)
            {
                CONEX_BD = "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=C:\\BD\\bd_usu_test.mdf;Integrated Security=True;Connect Timeout=30";
            }
            else {
                CONEX_BD = "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=C:\\BD\\bd_usu.mdf;Integrated Security=True;Connect Timeout=30";
            }
            ModeloUsuario.Instancia.delegadoCrear = Crear;
            ModeloUsuario.Instancia.delegadoEliminar = Eliminar;
            ModeloUsuario.Instancia.leerTodo = Leer;
            ModeloUsuario.Instancia.leerUno = LeerUno;
            ModeloUsuario.Instancia.leerConfirmar = LeerConfirmar;
            ModeloUsuario.Instancia.modificar = Modificar;
            listaUsuarios = Leer();
        }
        public void Crear(Usuario usuario)
        {
            if (!UsuarioEsValido(usuario)) return;

            bool existe = false;
            foreach(Usuario usu in listaUsuarios)
            {
                if(usu.Id == usuario.Id)
                {
                    existe = true;
                    break;
                } else
                {
                    existe = false;
                }
            }
            if (!existe) {
                try
                {
                    SqlConnection conexion;

                    using (conexion = new SqlConnection(CONEX_BD))
                    {
                        conexion.Open();

                        int aux = usuario.Activo ? 1 : 0;

                        SqlCommand comando = conexion.CreateCommand();
                        comando.CommandText = "INSERT INTO Usuario (nombre, email, altura, edad, activo) VALUES (@nombre, @email, @altura, @edad, @activo)";
                        comando.Parameters.AddWithValue("@nombre", usuario.Nombre);
                        comando.Parameters.AddWithValue("@email", usuario.Nombre.ToLower().Replace(' ', '_') + "@email.es");
                        comando.Parameters.AddWithValue("@altura", usuario.Altura);
                        comando.Parameters.AddWithValue("@edad", usuario.Edad);
                        comando.Parameters.AddWithValue("@activo", aux);
                        Console.WriteLine(comando.CommandText);
                        int filasAfectadas = comando.ExecuteNonQuery();
                        if (filasAfectadas != 1)
                            throw new Exception("No ha hecho insert bbdd " + comando.CommandText);
                    }   // conexion.Close();
                }
                catch (Exception ex)
                {
                    Console.Error.WriteLine("Error leyendo bbdd: " + ex.Message);
                }
            }
        }
        
        public void Guardar(List<Usuario> usus)
        {
            try
            {
                SqlConnection conexion;

                using (conexion = new SqlConnection(CONEX_BD))
                {
                    conexion.Open();

                    foreach (Usuario u in listaUsuarios)
                    {
                        SqlCommand comando = conexion.CreateCommand();
                        comando.CommandText = "INSERT INTO Usuario (nombre, email, altura, edad, activo) VALUES ('" 
                            + u.Nombre + "', '" 
                            + u.Nombre.ToLower().Replace(' ', '_') 
                            + "@email.es', "
                            + u.Altura.ToString() + ", "
                            + u.Edad + ", 0)";
                        int filasAfectadas = comando.ExecuteNonQuery();
                        if (filasAfectadas != 1)
                            throw new Exception("No ha hecho update bbdd " 
                                + comando.CommandText);
                    }
                }   // conexion.Close();
                Console.WriteLine("Guardar || Leidos de bbdd: " + listaUsuarios.Count);
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("Error leyendo bbdd: " + ex.Message);
            }
        }
        public bool Eliminar(int id)
        {
            if (LeerUno(id) != null)
            {
                try
                {
                    SqlConnection conexion;

                    using (conexion = new SqlConnection(CONEX_BD))
                    {
                        conexion.Open();

                        SqlCommand comando = conexion.CreateCommand();
                        comando.CommandText = "DELETE FROM Usuario WHERE Id = @id";
                        comando.Parameters.AddWithValue("@id", id);
                        int filasAfectadas = comando.ExecuteNonQuery();
                        if (filasAfectadas == 1)
                        {
                            if (LeerUno(id) == null)
                                return true;
                            else
                                return false;
                        }
                        else
                        {
                            Console.WriteLine("No ha hecho delete bbdd " + comando.CommandText);
                            return false;
                        }
                    }   // conexion.Close();
                }
                catch (Exception ex)
                {
                    Console.Error.WriteLine("Error leyendo bbdd: " + ex.Message);
                    return false;
                }
            }
            Console.Error.WriteLine("No se encontro el usuario a eliminar");
            return false;
        }
        public  List<Usuario> Leer()
        {
            listaUsuarios = new List<Usuario>();
            try
            {
                SqlConnection conexion;

                using (conexion = new SqlConnection(CONEX_BD))
                {
                    conexion.Open();

                    Usuario usuario;
                    SqlCommand comando = conexion.CreateCommand();
                    comando.CommandText = "SELECT id, nombre, altura, edad, activo FROM Usuario";
                    SqlDataReader lectorDR = comando.ExecuteReader();
                    
                    while (lectorDR.Read())
                    {
                        usuario = new Usuario();
                        usuario.Id = lectorDR.GetInt32(0);
                        usuario.Nombre = lectorDR[1].ToString();
                        usuario.Altura = (float) (double) lectorDR["altura"];
                        usuario.Edad = lectorDR.GetByte(3);
                        usuario.Activo = lectorDR.GetBoolean(4);

                        listaUsuarios.Add(usuario);
                    }
                }   // conexion.Close();
                Console.WriteLine("Leidos de bbdd: " + listaUsuarios.Count);
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("Error leyendo bbdd: " + ex.Message);
            }            
            return listaUsuarios;
        }

        public IList<Usuario> LeerTodo()
        {
            return listaUsuarios;
        }

        public Usuario LeerUno(int id)
        {
            Usuario usuario = null;
            try
            {
                SqlConnection conexion;

                using (conexion = new SqlConnection(CONEX_BD))
                {
                    conexion.Open();

                    SqlCommand comando = conexion.CreateCommand();
                    comando.CommandText = "SELECT id, nombre, altura, edad, activo FROM Usuario WHERE id = @id";
                    comando.Parameters.AddWithValue("@id", id);
                    SqlDataReader lectorDR = comando.ExecuteReader();

                    while (lectorDR.Read())
                    {
                        usuario = new Usuario();
                        usuario.Id = lectorDR.GetInt32(0);
                        usuario.Nombre = lectorDR[1].ToString();
                        usuario.Altura = (float)(double)lectorDR["altura"];
                        usuario.Edad = lectorDR.GetByte(3);
                        usuario.Activo = lectorDR.GetBoolean(4);
                    }
                }   // conexion.Close();
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("Error leyendo bbdd: " + ex.Message);
            }
            return usuario;
        }

        public bool LeerConfirmar(string nombre)
        {
            int posicion = 0;
            for (int i = 0; i < listaUsuarios.Count; i++)
            {
                if (listaUsuarios[i].GetNombre() == nombre)
                {
                    posicion = i;
                    return true;
                }
            }
            return false;
        }

        public void Modificar(Usuario usu)
        {
            if (!UsuarioEsValido(usu)) return;

            if (LeerUno((int) usu.Id) != null)
            {
                try
                {
                    SqlConnection conexion;

                    using (conexion = new SqlConnection(CONEX_BD))
                    {
                        conexion.Open();

                        int aux = usu.Activo ? 1 : 0;

                        SqlCommand comando = conexion.CreateCommand();
                        comando.CommandText = "UPDATE Usuario " +
                            "SET Nombre = @nombre, Email = @email, Altura = @altura, Edad = @edad, Activo = @activo WHERE Id = @id";
                        comando.Parameters.AddWithValue("@nombre", usu.Nombre);
                        comando.Parameters.AddWithValue("@email", usu.Nombre.ToLower().Replace(' ', '_') + "@email.es");
                        comando.Parameters.AddWithValue("@altura", usu.Altura.ToString().Replace(",", "."));
                        comando.Parameters.AddWithValue("@edad", usu.Edad);
                        comando.Parameters.AddWithValue("@activo", aux);
                        comando.Parameters.AddWithValue("@id", usu.Id);
                        int filasAfectadas = comando.ExecuteNonQuery();
                        if (filasAfectadas != 1)
                            throw new Exception("No ha hecho update bbdd " + comando.CommandText);
                    }   
                }
                catch (Exception ex)
                {
                    Console.Error.WriteLine("Error leyendo bbdd: " + ex.Message);
                }
            } else
            {
                Console.WriteLine("No se encontro el usuario a modificar");
            }
        }

        public bool UsuarioEsValido(Usuario u)
        {
            if (u.Nombre == "") return false;
            if (u.Edad < 1) return false;
            if (u.Edad > 200) return false;
            if (u.Altura < 0.5f) return false;
            if (u.Altura > 3f) return false;
            
            return true;

        }
    }
}
