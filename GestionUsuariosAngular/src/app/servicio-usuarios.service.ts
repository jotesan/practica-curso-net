import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Usuario } from './usuario';

@Injectable({
  providedIn: 'root'
})
export class ServicioUsuariosService {

  private url : string = "http://localhost:24918/leerTodos";
  private usuarios : Array<Usuario> = [];

  constructor(private clientHTTP : HttpClient) { 

    let observ : Observable<Array<Usuario>>; 
    observ = this.clientHTTP.get<Array<Usuario>>(this.url);
    observ.subscribe ( (datos) => {
      this.usuarios = datos;
    });

  }

  public arrayUsuarios() : Array<Usuario>{
    return this.usuarios;
  }

//////// Activos

  public usuariosActivos(): Array<Usuario> {
    let activos: Array<Usuario> = [];
    for (let usuario of this.usuarios) {
      if (usuario.activo) {
        activos.push(usuario);
      }
    }
    return activos;
  }

  public usuariosInactivos(): Array<Usuario> {
    let inactivos: Array<Usuario> = [];
    for (let usuario of this.usuarios) {
      if (!usuario.activo) {
        inactivos.push(usuario);
      }
    }
    return inactivos;
  }

//////// Edad
  
  public usuariosMenores() : Array<Usuario>{

    let usuariosMenores = [];

    for (let u of this.usuarios){
      if (u.edad<18){
        usuariosMenores.push(u);
      }
    }
    return usuariosMenores;
  }
  
  public usuariosMayores() : Array<Usuario>{

    let usuariosMayores = [];

    for (let u of this.usuarios){
      if (u.edad>=18){
        usuariosMayores.push(u);
      }
    }
    return usuariosMayores;
  }
}
