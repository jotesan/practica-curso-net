class Vista {

    constructor() {
        this.controlador = new Controlador(this);
        this.idUsuarioModificado = -1;
        this.pNombre = document.getElementById("errorNombre");
        this.pEdad = document.getElementById("errorEdad");
        this.pAltura = document.getElementById("errorAltura");
        this.regexpNombre = /^([A-Za-zÑñ]){1,50}(\s([A-Za-zÑñ]){1,50}){0,3}$/;
        this.regexpEdad = /^([0-9])+$/;
        this.regexpAltura = /^([0-9])+(.?([0-9])+)?$/;
        this.btnAnadir = document.getElementById("btn-anadir");
        this.btnModificar = document.getElementById("btn-modificar");
        this.btnAnadir.addEventListener("click", () => this.crearUsuario());
        this.btnModificar.addEventListener("click", () => this.modificarUsuario());
        document.getElementById("btn-borrar-todo").addEventListener("click", () => {
            console.error("No tienes permisos para borrar todo");
        });
        this.controlador.leerTodosUsuarios();
    }

    comporbarCampos(nombre, edad, altura) {
        let bien = true;

        if(!this.regexpNombre.test(nombre)) {
            this.pNombre.classList.remove("oculto");
            this.pNombre.classList.add("visible");
            this.pNombre.classList.add("letrasRojas");
            this.pNombre.innerHTML = "<br>El nombre solo puede contener letras";
            bien = false;
        } else  {
            this.pNombre.className = "oculto";
        }
        if(!this.regexpEdad.test(edad) || edad < 1 || edad > 122) {
            this.pEdad.classList.remove("oculto");
            this.pEdad.classList.add("visible");
            this.pEdad.classList.add("letrasRojas");
            this.pEdad.innerHTML = "<br>La edad solo puede contener numeros enteros";
            bien = false;
        } else  {
            this.pEdad.className = "oculto";
        }
        if(!this.regexpAltura.test(altura) || altura < 0.54 || altura > 2.72) {
            this.pAltura.classList.remove("oculto");
            this.pAltura.classList.add("visible");
            this.pAltura.classList.add("letrasRojas");
            this.pAltura.innerHTML = "<br>La altura solo puede contener numeros enteros o decimales";
            bien = false;
        } else  {
            this.pAltura.className = "oculto";
        }

        return bien;
    }

    // mostrarUsuarios() {
        
    //     let usuarios = this.controlador.leerTodosUsuarios();
    //     let tbody = document.getElementById("tbody-usuarios");
    //     tbody.innerHTML = "";
    //     let activo = "";
    //     for (let i = 0; i < usuarios.length; i++) {

    //         console.log("Desde la vista: " + usuarios[i].nombre);

    //         if (usuarios[i].activo) {
    //             activo = "SI";
    //         } else {
    //             activo = "NO";
    //         }

    //         let tr = tbody.appendChild(document.createElement("tr"));
    //         tr.id = `usuario${i}`;
    //         tr.appendChild(document.createElement("td"))
    //             .appendChild(document.createTextNode(usuarios[i].nombre));
    //         tr.appendChild(document.createElement("td"))
    //             .appendChild(document.createTextNode(usuarios[i].edad));
    //         tr.appendChild(document.createElement("td"))
    //             .appendChild(document.createTextNode(usuarios[i].altura));
    //         tr.appendChild(document.createElement("td"))
    //             .appendChild(document.createTextNode(activo));
    //         let btnModificar = tr.appendChild(document.createElement("td"))
    //             .appendChild(document.createElement("input"));
    //         btnModificar.id = `btn-modificar${i}`;
    //         btnModificar.type = "button";
    //         btnModificar.value = "M";
    //         btnModificar.classList.add("button");
    //         btnModificar.classList.add("button2");
    //         document.getElementById(`btn-modificar${i}`).addEventListener("click", () => this.modificar(usuarios[i].id));
    //         let tdEliminar = tr.appendChild(document.createElement("td"));
    //         let btnEliminar = tdEliminar.appendChild(document.createElement("input"));
    //         btnEliminar.id = `btn-eliminar${i}`;
    //         btnEliminar.type = "button";
    //         btnEliminar.value = "X";
    //         btnEliminar.classList.add("button");
    //         btnEliminar.classList.add("button3");
    //         btnEliminar.classList.add("visible");

    //         let btnAceptar = tdEliminar.appendChild(document.createElement("input"));
    //         btnAceptar.id = `btn-aceptar${i}`;
    //         btnAceptar.type = "button";
    //         btnAceptar.value = "Aceptar";
    //         btnAceptar.classList.add("button");
    //         btnAceptar.classList.add("oculto");

    //         let btnCancelar = tdEliminar.appendChild(document.createElement("input"));
    //         btnCancelar.id = `btn-cancelar${i}`;
    //         btnCancelar.type = "button";
    //         btnCancelar.value = "Cancelar";
    //         btnCancelar.classList.add("button");
    //         btnCancelar.classList.add("button3");
    //         btnCancelar.classList.add("oculto");

    //         btnEliminar = document.getElementById(`btn-eliminar${i}`);
    //         btnAceptar = document.getElementById(`btn-aceptar${i}`);
    //         btnCancelar = document.getElementById(`btn-cancelar${i}`);

    //         btnAceptar.addEventListener("click", () => {this.eliminarUsuario(usuarios[i].id)});

    //         btnCancelar.addEventListener("click", () => {
    //             btnAceptar.classList.remove("visible");
    //             btnAceptar.classList.add("oculto");
    //             btnCancelar.classList.remove("visible");
    //             btnCancelar.classList.add("oculto");
    //             btnEliminar.classList.remove("oculto");
    //             btnEliminar.classList.add("visible");
    //         });

    //         btnEliminar.addEventListener("click", () => {
    //             btnEliminar.classList.remove("visible");
    //             btnEliminar.classList.add("oculto");
    //             btnAceptar.classList.remove("oculto");
    //             btnAceptar.classList.add("visible");
    //             btnCancelar.classList.remove("oculto");
    //             btnCancelar.classList.add("visible");
    //         });
    //     }
    // }

    crearUsuario() {
        let nombre = document.getElementById("nombre").value;
        let edad = document.getElementById("edad").value;
        let altura = document.getElementById("altura").value;
        let activo = document.getElementById("activo").checked;
        if(!this.comporbarCampos(nombre, edad, altura)) return;
        let usuarioNuevo = new Usuario(nombre, parseInt(edad), parseFloat(altura), activo);
        this.pNombre.className = "oculto";
        this.pEdad.className = "oculto";
        this.pAltura.className = "oculto";
        this.controlador.crearUsuario(usuarioNuevo);
        // window.location.replace(window.location);
        // this.actualizarPagina();
    }

    modificar(i,id) {
        let nombreUsuario = document.getElementById("nombre");
        let edadUsuario = document.getElementById("edad");
        let alturaUsuario = document.getElementById("altura");
        let activoUsuario = document.getElementById("activo");
        let trUsuario = document.getElementById(`usuario${i}`);
        nombreUsuario.value = trUsuario.children[0].textContent;
        edadUsuario.value = trUsuario.children[1].textContent;
        alturaUsuario.value = trUsuario.children[2].textContent;
        let activo = trUsuario.children[3].textContent;
        if (activo == "SI") {
            activoUsuario.checked = true;
        } else {
            activoUsuario.checked = false;
        }
        this.btnModificar.classList.remove("oculto");
        this.btnModificar.classList.add("visible");
        this.btnModificar.classList.add("button");
        this.btnModificar.classList.add("button4");
        this.btnAnadir.classList.remove("visible");
        this.btnAnadir.classList.add("oculto");
        this.idUsuarioModificado = id;
    }

    modificarUsuario() {
        let nombreNuevo = document.getElementById("nombre").value;
        let edadNueva = document.getElementById("edad").value;
        let alturaNueva = document.getElementById("altura").value;
        let activoNuevo = document.getElementById("activo").checked;
        if(!this.comporbarCampos(nombreNuevo, edadNueva, alturaNueva)) return;
        let usuarioModificado = new Usuario(nombreNuevo, parseInt(edadNueva), parseFloat(alturaNueva), activoNuevo);
        this.pNombre.className = "oculto";
        this.pEdad.className = "oculto";
        this.pAltura.className = "oculto";
        this.controlador.modificarUsuario(this.idUsuarioModificado, usuarioModificado);
        this.btnModificar.className = "oculto";
        this.btnAnadir.classList.remove("oculto");
        this.btnAnadir.classList.add("visible");
    }

    eliminarUsuario(id) {
        
        this.controlador.eliminarUsuario(id);
    }

    actualizarPagina(lista_usuarios){

        let nombreNuevo = document.getElementById("nombre");
        let edadNueva = document.getElementById("edad");
        let alturaNueva = document.getElementById("altura");
        let activoNuevo = document.getElementById("activo");

        nombreNuevo.value = "";
        edadNueva.value = "";
        alturaNueva.value = "";
        activoNuevo.checked = false;

        nombreNuevo.placeholder = "Nombre";
        edadNueva.placeholder = "Edad";
        alturaNueva.placeholder = "Altura";

        let usuarios = lista_usuarios;
        let tbody = document.getElementById("tbody-usuarios");
        tbody.innerHTML = "";
        let activo = "";
        for (let i = 0; i < usuarios.length; i++) {
            if (usuarios[i].activo) {
                activo = "SI";
            } else {
                activo = "NO";
            }

            let tr = tbody.appendChild(document.createElement("tr"));
            tr.id = `usuario${i}`;
            tr.appendChild(document.createElement("td"))
                .appendChild(document.createTextNode(usuarios[i].nombre));
            tr.appendChild(document.createElement("td"))
                .appendChild(document.createTextNode(usuarios[i].edad));
            tr.appendChild(document.createElement("td"))
                .appendChild(document.createTextNode(usuarios[i].altura));
            tr.appendChild(document.createElement("td"))
                .appendChild(document.createTextNode(activo));
            let btnModificar = tr.appendChild(document.createElement("td"))
                .appendChild(document.createElement("input"));
            btnModificar.id = `btn-modificar${i}`;
            btnModificar.type = "button";
            btnModificar.value = "M";
            btnModificar.classList.add("button");
            btnModificar.classList.add("button2");
            document.getElementById(`btn-modificar${i}`).addEventListener("click", () => this.modificar(i, usuarios[i].id));
            let tdEliminar = tr.appendChild(document.createElement("td"));
            let btnEliminar = tdEliminar.appendChild(document.createElement("input"));
            btnEliminar.id = `btn-eliminar${i}`;
            btnEliminar.type = "button";
            btnEliminar.value = "X";
            btnEliminar.classList.add("button");
            btnEliminar.classList.add("button3");
            btnEliminar.classList.add("visible");

            let btnAceptar = tdEliminar.appendChild(document.createElement("input"));
            btnAceptar.id = `btn-aceptar${i}`;
            btnAceptar.type = "button";
            btnAceptar.value = "Aceptar";
            btnAceptar.classList.add("button");
            btnAceptar.classList.add("oculto");

            let btnCancelar = tdEliminar.appendChild(document.createElement("input"));
            btnCancelar.id = `btn-cancelar${i}`;
            btnCancelar.type = "button";
            btnCancelar.value = "Cancelar";
            btnCancelar.classList.add("button");
            btnCancelar.classList.add("button3");
            btnCancelar.classList.add("oculto");

            btnEliminar = document.getElementById(`btn-eliminar${i}`);
            btnAceptar = document.getElementById(`btn-aceptar${i}`);
            btnCancelar = document.getElementById(`btn-cancelar${i}`);

            btnAceptar.addEventListener("click", () => {this.eliminarUsuario(usuarios[i].id)});

            btnCancelar.addEventListener("click", () => {
                btnAceptar.classList.remove("visible");
                btnAceptar.classList.add("oculto");
                btnCancelar.classList.remove("visible");
                btnCancelar.classList.add("oculto");
                btnEliminar.classList.remove("oculto");
                btnEliminar.classList.add("visible");
            });

            btnEliminar.addEventListener("click", () => {
                btnEliminar.classList.remove("visible");
                btnEliminar.classList.add("oculto");
                btnAceptar.classList.remove("oculto");
                btnAceptar.classList.add("visible");
                btnCancelar.classList.remove("oculto");
                btnCancelar.classList.add("visible");
            });
        }

    }
}

window.addEventListener("load", function () {
    let vista = new Vista();
});