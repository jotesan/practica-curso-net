﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Practica02_PatronesDeDiseño
{
    public interface IEditableConsola
    {

        public void MostrarDatosConsola();

        public void PedirDatosConsola();

    }
}
