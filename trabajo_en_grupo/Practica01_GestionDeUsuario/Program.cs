﻿using System;

namespace Practica01_GestionDeUsuario
{
    class Program
    {
        static void Main(string[] args)
        {

            int res = 0;
            string str;
            int id;
            GestionUsuario gestor = new GestionUsuario();

            while (res != 7)
            {
                Console.WriteLine("\n¿Que desea hacer?");
                Console.WriteLine("1.- Crear un usuario");
                Console.WriteLine("2.- Mostrar usuario");
                Console.WriteLine("3.- Mostrar todos");
                Console.WriteLine("4.- Modificar un usuario");
                Console.WriteLine("5.- Eliminar un usuario");
                Console.WriteLine("6.- Limpiar pantalla");
                Console.WriteLine("\n7.- Salir\n");

                res = int.Parse(Console.ReadLine());

                switch (res)
                {
                    case 1:
                        gestor.CrearUno();
                        break;
                    case 2:
                        Console.WriteLine("introduzca el id o el nombre del usuario que desea mostrar");

                        str = Console.ReadLine();

                        if( int.TryParse(str, out id))
                        {
                            gestor.MostrarUno<int>(id);
                        }
                        else
                        {
                            gestor.MostrarUno<string>(str);
                        }

                        break;
                    case 3:
                        gestor.MostrarTodos();
                        break;
                    case 4:
                        Console.WriteLine("introduzca el id o el nombre del usuario que desea modificar");

                        str = Console.ReadLine();

                        if (int.TryParse(str, out id))
                        {
                            gestor.ModificarUno<int>(id);
                        }
                        else
                        {
                            gestor.ModificarUno<string>(str);
                        }
                        break;
                    case 5:
                        Console.WriteLine("introduzca el id o el nombre del usuario que desea eliminar");

                        str = Console.ReadLine();

                        if (int.TryParse(str, out id))
                        {
                            gestor.EliminarUno<int>(id);
                        }
                        else
                        {
                            gestor.EliminarUno<string>(str);
                        }
                        break;
                    case 6:
                        Console.Clear();
                        break;
                    case 7:
                        Console.WriteLine("Adios");
                        break;
                    default:
                        Console.WriteLine("Numero invalido, vuelta a probar");
                        break;
                }

            }

        }
    }
}
