class Controlador {

    constructor(vista) {
        this.modelo = new Modelo();
        this.modelo.onChange = (usuarios) => {
            vista.actualizarPagina(usuarios);
        }
    }

    crearUsuario(usuario){
       this.modelo.crearUsuario(usuario);
    }

    eliminarUsuario(id){
        this.modelo.eliminarUsuario(id);
    }

    leerTodosUsuarios(){

        this.modelo.leerTodosUsuarios();
        return;
    }

    modificarUsuario(id,usuario){
        this.modelo.modificarUsuario(id,usuario);
    }
}

