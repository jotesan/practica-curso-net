class Modelo{
    
    constructor(){

        let usuariosGuardados = JSON.parse(window.localStorage.getItem("usuarios"));
        if (usuariosGuardados === null) {
            usuariosGuardados = [];
        }
        this.usuarios = usuariosGuardados;
        this.onChange = null;
    }

    crearUsuario(usuario){
        async function enviaJSONPost(u){

            let opcionesPOST = {
                method: "POST",
                mode: "cors",
                cache: "no-cache",
                headers: {
                    "Content-Type": "application/json"
                },
                body: `{
                    "nombre": "${u.nombre}",
                    "edad": ${u.edad},
                    "altura": ${u.altura},
                    "activo": ${u.activo}
                }`
            }
            await fetch("http://localhost:21902/crear", opcionesPOST);
        };
        enviaJSONPost(usuario).then(() => {
            this.leerTodosUsuarios();
        });
    }

    eliminarUsuario(id){

        async function enviaJSONPost(id){

            let opcionesDELETE = {
                method: "DELETE",
                mode: "cors",
                cache: "no-cache",
                headers: {
                    "Content-Type": "application/json"
                }
            }
            await fetch("http://localhost:21902/eliminar/"+id, opcionesDELETE);
        };

        enviaJSONPost(id).then(() => {
            this.leerTodosUsuarios();
        });
    }

    leerTodosUsuarios(){

        let promesaAJAX = fetch("http://localhost:21902/leerTodos");
        this.usuarios = []
        promesaAJAX.then((respuesta) => respuesta.json())
        .then(lista_users => {
            for (let user of lista_users) {
                this.usuarios.push(user);
            }
            this.onChange(this.usuarios);
        });
    }

    modificarUsuario(id,usuario){

        async function enviaJSONPost(u,id){

            let opcionesPUT = {
                method: "PUT",
                mode: "cors",
                cache: "no-cache",
                headers: {
                    "Content-Type": "application/json"
                },
                body: `{
                    "id": ${id},
                    "nombre": "${u.nombre}",
                    "edad": ${u.edad},
                    "altura": ${u.altura},
                    "activo": ${u.activo}
                }`
            }
            await fetch("http://localhost:21902/modificar", opcionesPUT);
        };
        enviaJSONPost(usuario,id).then(() => {
            this.leerTodosUsuarios();
        });
    }
}


