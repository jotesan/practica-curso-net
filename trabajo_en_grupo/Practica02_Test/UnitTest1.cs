using NUnit.Framework;
using Practica02_PatronesDeDiseņo;
using System;
using System.IO;
using System.Reflection;

namespace Practica02_Test
{
    public class Tests
    {
        ControladorUsuario cu;
        string nombreFichero = "FicheroPruebas.json";

        [SetUp]
        public void Setup()
        {

            cu = new ControladorUsuario();
            Persistencia.AsignarAccion(nombreFichero);
            Persistencia.GetUsers(nombreFichero);
            EliminarTodosLosUsuarios();

        }

        private void EliminarFicheroPruebas()
        {
            if (File.Exists("../../../" + nombreFichero))
            {
                File.Delete("../../../" + nombreFichero);
            }
            else
            {
                Console.WriteLine("No existe el fichero");
            }
                
        }

        private void EliminarTodosLosUsuarios()
        {
            int n = cu.LeerTodos().Count;

            for (int i = 0; i < n; i++)
            {
                cu.Eliminar(0);
            }
        }


        private void ResetearModeloYControlador()
        {
            //FieldInfo[] camposModelo = ModeloUsuario.Instancia.GetType().GetFields(...);
            FieldInfo[] camposModelo = typeof(ModeloUsuario).GetFields(
                BindingFlags.NonPublic | BindingFlags.Static);
            foreach (FieldInfo campo in camposModelo)
            {
                if (campo.Name.Equals("instancia"))
                {
                    Console.WriteLine("Encontrado instance Modelo y eliminado");
                    campo.SetValue(null, null);
                }
            }
            cu = new ControladorUsuario();
            Persistencia.AsignarAccion(nombreFichero);
            Persistencia.GetUsers(nombreFichero);
        }

        [Test]
        public void TestCrearUsuarios()
        {
            cu.Crear(new Usuario("Pepe", 30, 1.5f));
            cu.Crear(new Usuario("Ana", 24, 1.68f));
            cu.Crear(new Usuario("Jose", 15, 1.72f));
            cu.Crear(new Usuario("Paula", 50, 1.8f)); 

            Assert.AreEqual(cu.LeerTodos().Count, 4);
        }

        [Test]
        public void TestModificarUsuarios()
        {

            cu.Crear(new Usuario("Pepe", 30, 1.5f));
            cu.Crear(new Usuario("Ana", 24, 1.68f));
            cu.Crear(new Usuario("Jose", 15, 1.72f));

            Assert.NotNull(cu.Modificar("Pepe", "Edad", 24));
            Assert.NotNull(cu.Modificar("Ana", "Altura", 1.7f));
            Assert.NotNull(cu.Modificar("Jose", "Nombre", "Juan"));

            Assert.AreEqual(cu.LeerTodos().Count, 3);
        }

        [Test]
        public void TestLeerUsuarios()
        {
            TestCrearUsuarios();
            Assert.NotNull(cu.LeerUno("Jose"));
            Assert.NotNull(cu.LeerTodos());
            Assert.AreEqual(cu.LeerTodos().Count, 4);
        }


        [Test]
        public void TestEliminarUsuarioExistenteNombre()
        {
            cu.Crear(new Usuario("Pepe", 30, 1.5f));
            cu.Crear(new Usuario("Ana", 24, 1.68f));
            cu.Crear(new Usuario("Jose", 15, 1.72f));

            Assert.IsTrue(cu.Eliminar("Pepe"));

            Assert.AreEqual(cu.LeerTodos().Count, 2);
        }

        [Test]
        public void TestEliminarUsuarioExistentePosicion()
        {
            cu.Crear(new Usuario("Pepe", 30, 1.5f));
            cu.Crear(new Usuario("Ana", 24, 1.68f));
            cu.Crear(new Usuario("Jose", 15, 1.72f));

            Assert.IsTrue(cu.Eliminar(0));

            Assert.AreEqual(cu.LeerTodos().Count, 2);
        }

        [Test]
        public void TestEliminarUsuarioNoExistenteNombre()
        {
            cu.Crear(new Usuario("Pepe", 30, 1.5f));
            cu.Crear(new Usuario("Ana", 24, 1.68f));
            cu.Crear(new Usuario("Jose", 15, 1.72f));

            Assert.IsFalse(cu.Eliminar("Carla"));

            Assert.AreEqual(cu.LeerTodos().Count, 3);
        }

        [Test]
        public void TestEliminarUsuarioNoExistentePosicion()
        {
            cu.Crear(new Usuario("Pepe", 30, 1.5f));
            cu.Crear(new Usuario("Ana", 24, 1.68f));
            cu.Crear(new Usuario("Jose", 15, 1.72f));

            Assert.IsFalse(cu.Eliminar(10));

            Assert.AreEqual(cu.LeerTodos().Count, 3);
        }

        [Test]
        public void TestEscrituraFichero()
        {

            cu.Crear(new Usuario("Pepe", 30, 1.5f));
            cu.Crear(new Usuario("Ana", 24, 1.68f));
            cu.Crear(new Usuario("Jose", 15, 1.72f));

            ResetearModeloYControlador();

            Assert.AreEqual(cu.LeerTodos().Count, 3);

        }

        [Test]
        public void TestEscrituraFicheroNoExistente()
        {
            EliminarFicheroPruebas();

            cu.Crear(new Usuario("Pape", 30, 1.5f));
            cu.Crear(new Usuario("Ona", 24, 1.68f));
            cu.Crear(new Usuario("Jese", 15, 1.72f));

            ResetearModeloYControlador();

            Assert.AreEqual(cu.LeerTodos().Count, 3);

        }
    }
}