﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.Json;

namespace Practica02_PatronesDeDiseño
{
    public static class Persistencia
    {

        public static void AsignarAccion(string nombre_fichero)
        {
            ModeloUsuario.Instancia.OnChange = () =>
            {
                JsonSerializerOptions jso = new JsonSerializerOptions();
                jso.WriteIndented = true;
                string userS = JsonSerializer.Serialize(ModeloUsuario.Instancia.LeerTodos(), jso);
                File.WriteAllText("../../../" + nombre_fichero, userS);
            };

        }

        public static void GetUsers(string nombre_fichero)
        {
            if (File.Exists("../../../" + nombre_fichero))
            {
                List<Usuario> lista = new List<Usuario>();
                string jsonUsers = File.ReadAllText("../../../" + nombre_fichero);
                lista = JsonSerializer.Deserialize<List<Usuario>>(jsonUsers);
                foreach (Usuario u in lista)
                {
                    ModeloUsuario.Instancia.Crear(u);
                }
            }
        }
    }
}
