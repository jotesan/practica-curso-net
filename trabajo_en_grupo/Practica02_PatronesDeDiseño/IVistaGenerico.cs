﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Practica02_PatronesDeDiseño
{
    public interface IVistaGenerico
    {
        void Menu();

        void Alta();

        void Baja();

        void Modificar();

        void MostrarUno();

        void MostrarTodos();

    }
}
