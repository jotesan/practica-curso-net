import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UsuariosAdultosComponent } from './usuarios-adultos.component';

describe('UsuariosAdultosComponent', () => {
  let component: UsuariosAdultosComponent;
  let fixture: ComponentFixture<UsuariosAdultosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UsuariosAdultosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UsuariosAdultosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
