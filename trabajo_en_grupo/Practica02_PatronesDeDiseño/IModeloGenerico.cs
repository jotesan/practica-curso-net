﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Practica02_PatronesDeDiseño
{
    public interface IModeloGenerico<Tipo>
    {

        bool Crear(Tipo nuevoOb);

        IList<Tipo> LeerTodos();

        Tipo LeerUno<TBusqueda>(TBusqueda dato);

        bool Eliminar<TBusqueda>(TBusqueda dato);

        Tipo Modificar<TBusqueda,TCampo>(TBusqueda dato, string nombre,TCampo campo);

    }
}
