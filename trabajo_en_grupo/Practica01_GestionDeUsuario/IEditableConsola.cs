﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Practica01_GestionDeUsuario
{
    public interface IEditableConsola
    {

        public void MostrarDatosConsola();

        public void PedirDatosConsola();

    }
}
