
using ModeloUsuarios;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace Practica03_Test
{
    public class Tests
    {
        IWebDriver driver;
        IWebElement campo_activo;
        IWebElement campoNombre;
        IWebElement campoEdad;
        IWebElement campoAltura;
        IWebElement btnAnadir;
        IWebElement tabla;

        Process _process;

        int n_usuarios_iniciales;

        private void StartServer()
        {
            string path = "../../../../UsuariosWeb_APIRest/UsuariosWeb_APIRest.csproj";
            string applicationPath = Path.GetFullPath(path);

            _process = new Process
            {
                StartInfo =
                {
                    FileName = @"dotnet.exe",
                    Arguments = "run --project " + applicationPath
                }
            };
            _process.Start();
        }

        [OneTimeSetUp]
        public void InicializarClaseTest()
        {
            StartServer();

            string fichFirefox = "C:/Users/pmpcurso1/AppData/Local/Mozilla Firefox/firefox.exe";

            if (File.Exists(fichFirefox))
            {
                FirefoxDriverService geckoService = FirefoxDriverService.CreateDefaultService("./");
                geckoService.Host = "::1";

                FirefoxOptions firefoxoptions = new FirefoxOptions();
                firefoxoptions.BrowserExecutableLocation = fichFirefox;
                firefoxoptions.AcceptInsecureCertificates = true;

                driver = new FirefoxDriver(geckoService, firefoxoptions);

            }

            AbrirPaginaWeb();

            var campos = driver.FindElements(By.CssSelector("div[class='field']"));
            campoNombre = campos[0].FindElement(By.Id("nombre"));
            campo_activo = driver.FindElement(By.Id("activo"));
            campoEdad = campos[1].FindElement(By.Id("edad"));
            campoAltura = campos[2].FindElement(By.Id("altura"));
            btnAnadir = driver.FindElement(By.Id("btn-anadir"));
            tabla = driver.FindElement(By.TagName("table"));

            var tr_usuarios = tabla.FindElements(By.TagName("tr"));
            n_usuarios_iniciales = tr_usuarios.Count - 1;
        }


        [OneTimeTearDown]

        public void FinalizarClaseTest()
        {
            driver.Close();
            _process.Close();
            System.Diagnostics.Process.Start("taskkill", "/IM geckodriver.exe /F");   
        }


        [TearDown]

        public void FinalizarTestUnitario()
        {
            EliminarTodo();
        }

        public void Wait(int seg, int timeOut = 60)
        {
            var wait = new WebDriverWait(driver, new TimeSpan(0, 0, 1, timeOut));
            var delay = new TimeSpan(0, 0, 0, seg);
            var timeInic = DateTime.Now;
            wait.Until(driver => (DateTime.Now - timeInic > delay));
        }

        public void AbrirPaginaWeb()
        {
            string path = "http://localhost:5000/";
            //string ruta_absoluta = Path.GetFullPath(path);
            driver.Navigate().GoToUrl(path);
            string linkGestionUsuarios = driver.FindElements(By.TagName("a"))[4].GetAttribute("href");
            driver.Navigate().GoToUrl(linkGestionUsuarios);
        }

        public void InsertarUsuario(string nombre, int edad, float altura, bool activo)
        {

            campoNombre.Clear();
            campoEdad.Clear();
            campoAltura.Clear();

            campoNombre.SendKeys(nombre);
            campoEdad.SendKeys(edad.ToString());
            campoAltura.SendKeys(altura.ToString());
            if (activo)
                campo_activo.SendKeys(Keys.Space);


            Wait(1);

            btnAnadir.Click();
        }

        public void InsertarUsuarioInvalido(string nombre, string edad, string altura, bool activo)
        {
            campoNombre.Clear();
            campoEdad.Clear();
            campoAltura.Clear();

            campoNombre.SendKeys(nombre);
            campoEdad.SendKeys(edad);
            campoAltura.SendKeys(altura);
            if (activo)
                campo_activo.SendKeys(Keys.Space);

            Wait(1);

            btnAnadir.Click();
        }

        public void EliminarTodo()
        {
            var tr_usuarios = tabla.FindElements(By.TagName("tr"));
            IWebElement btn_eliminar;
            IWebElement btn_aceptar;
            int n = tr_usuarios.Count -1;
            for (int i = n_usuarios_iniciales; i < n; i++) //Empieza en 1 porque el primer tr es el encabezado
            {
                btn_eliminar = tabla.FindElement(By.Id("btn-eliminar"+n_usuarios_iniciales.ToString()));
                btn_eliminar.Click();
                btn_aceptar = tabla.FindElement(By.Id("btn-aceptar"+n_usuarios_iniciales.ToString()));
                btn_aceptar.Click();
            }
        }

        public void ModificarUsuario(int pos, string nombre, string edad, string altura)
        {
            var tr_usuarios = tabla.FindElements(By.TagName("tr"));
            int n = pos > tr_usuarios.Count ? tr_usuarios.Count : pos;

            var btnModificarTabla = driver.FindElement(By.Id("btn-modificar" + n.ToString()));
            btnModificarTabla.Click();

            campoNombre.Clear();
            campoEdad.Clear();
            campoAltura.Clear();
            campoNombre.SendKeys(nombre);
            campoEdad.SendKeys(edad);
            campoAltura.SendKeys(altura);
            campo_activo.SendKeys(Keys.Space);

            var btnModificar = driver.FindElement(By.Id("btn-modificar"));
            btnModificar.Click();
        }

        public void EliminarUno(int pos)
        {
            var tr_usuarios = tabla.FindElements(By.TagName("tr"));
            IWebElement btn_eliminar;
            IWebElement btn_aceptar;
            int n = pos>tr_usuarios.Count ? tr_usuarios.Count : pos;

            string id_eliminar = "btn-eliminar" + n.ToString();
            string id_aceptar = "btn-aceptar" + n.ToString();

            btn_eliminar = tabla.FindElement(By.Id(id_eliminar));
            btn_eliminar.Click();
            btn_aceptar = tabla.FindElement(By.Id(id_aceptar));
            btn_aceptar.Click();
            
        }

        public void CrearValidos()
        {
            InsertarUsuario("Juan", 20, 1.2f, true);
            InsertarUsuario("Julia", 35, 1.9f, false);
            InsertarUsuario("Jose", 41, 1.65f, true);
        }

        [Test]
        public void TestCrearUsuarios()
        {
            CrearValidos();

            //Wait(5);
            var tr_usuarios = tabla.FindElements(By.TagName("tr"));
            int n_usuario_validos = tr_usuarios.Count;
            Assert.GreaterOrEqual(n_usuario_validos, 2, "No se han insertado todos los usuarios");

            //Intentamos introducir un usuario con un nombre invalido
            InsertarUsuarioInvalido("12345","12","1.2f",true);
            tr_usuarios = tabla.FindElements(By.TagName("tr"));
            Assert.AreEqual(n_usuario_validos, tr_usuarios.Count, "Se ha insertado un usuario con un nombre invalido");

            //Intentamos introducir un usuario con una edad invalida
            InsertarUsuarioInvalido("Paco","-1","1.7f",true);
            tr_usuarios = tabla.FindElements(By.TagName("tr"));
            Assert.AreEqual(n_usuario_validos, tr_usuarios.Count, "Se ha insertado un usuario con una edad invalida");

            //Intentamos introducir un usuario con una edad invalida
            InsertarUsuarioInvalido("Paco","200","1.7f",true);
            tr_usuarios = tabla.FindElements(By.TagName("tr"));
            Assert.AreEqual(n_usuario_validos, tr_usuarios.Count, "Se ha insertado un usuario con una edad invalida");

            //Intentamos introducir un usuario con una altura invalida
            InsertarUsuarioInvalido("Paco","12","3.0f",true);
            tr_usuarios = tabla.FindElements(By.TagName("tr"));
            Assert.AreEqual(n_usuario_validos, tr_usuarios.Count, "Se ha insertado un usuario con una altura invalida");

            //Intentamos introducir un usuario con una altura invalida
            InsertarUsuarioInvalido("Paco","12","-1.0f",true);
            tr_usuarios = tabla.FindElements(By.TagName("tr"));
            Assert.AreEqual(n_usuario_validos, tr_usuarios.Count, "Se ha insertado un usuario con una altura invalida");

        }

        [Test]
        public void TestEliminarUsuarios()
        {
            CrearValidos();
            Wait(5);
            var tr_usuarios = tabla.FindElements(By.TagName("tr"));

            int n_usuarios = tr_usuarios.Count;

            IWebElement btn_eliminar = tabla.FindElement(By.Id("btn-eliminar" + (n_usuarios - 2).ToString()));
            btn_eliminar.Click();
            IWebElement btn_aceptar = tabla.FindElement(By.Id("btn-aceptar" + (n_usuarios - 2).ToString()));
            btn_aceptar.Click();

            tr_usuarios = tabla.FindElements(By.TagName("tr"));

            Assert.AreEqual(n_usuarios - 1, tr_usuarios.Count, "No se ha borrado el usuario");

        }

        [Test]
        public void TestEliminarTodos()
        {
            InsertarUsuario("Juanjo", 20, 1.2f, true);
            InsertarUsuario("Carla", 20, 1.2f, true);

            var tr_usuarios = tabla.FindElements(By.TagName("tr"));
            Assert.GreaterOrEqual(tr_usuarios.Count -1, n_usuarios_iniciales + 2 , "No se han introducido los usuarios");

            EliminarTodo();

            tr_usuarios = tabla.FindElements(By.TagName("tr"));
            Assert.AreEqual(tr_usuarios.Count - 1, n_usuarios_iniciales, "No se han eliminado todos los usuarios");

        }

        [Test]
        public void TestModificarUsuario()
        {
            CrearValidos();

            ModificarUsuario(n_usuarios_iniciales, "Modificado", "30", "1.7");
            Wait(1);
            var columnas = driver.FindElements(By.TagName("td"));
            Assert.AreEqual(columnas[n_usuarios_iniciales*6].Text, "Modificado");
            Assert.AreEqual(columnas[n_usuarios_iniciales * 6 + 1].Text, "30");
            Assert.AreEqual(columnas[n_usuarios_iniciales * 6 + 2].Text, "1.7");

        }

        //Devuelve los usuarios de la tabla almacenados en una lista
        public List<Usuario> LeerUsuariosTabla()
        {
            var tr_usuarios = tabla.FindElements(By.TagName("tr"));
            int n_usuario_validos = tr_usuarios.Count;
            List<Usuario> lista_usuarios = new List<Usuario>();

            string nombre;
            int edad;
            float altura;
            bool activo;

            for (int i = 1; i < n_usuario_validos; i++)
            {
                var td = tr_usuarios[i].FindElements(By.TagName("td"));
                nombre =  td[0].Text;
                edad = int.Parse(td[1].Text);
                altura = float.Parse(td[2].Text.Replace('.', ','));
                activo = td[3].Text == "SI" ? true : false ;

                lista_usuarios.Add(new Usuario(nombre, edad, altura, activo));
            }

            return lista_usuarios;
        }


        //Refresca la pagina y vuelve a inicializar los IWebElement que usamos
        public void RefrescarPagina()
        {
            driver.Navigate().Refresh();

            var campos = driver.FindElements(By.CssSelector("div[class='field']"));
            campoNombre = campos[0].FindElement(By.Id("nombre"));
            campo_activo = driver.FindElement(By.Id("activo"));
            campoEdad = campos[1].FindElement(By.Id("edad"));
            campoAltura = campos[2].FindElement(By.Id("altura"));
            btnAnadir = driver.FindElement(By.Id("btn-anadir"));
            tabla = driver.FindElement(By.TagName("table"));

        }

        [Test]

        public void ComprobarPersistenciaCrearUsuarios()
        {
            CrearValidos();

            var lista1 = LeerUsuariosTabla();
            RefrescarPagina();
            var lista2 = LeerUsuariosTabla();

            Assert.AreEqual(lista1.Count, lista2.Count, "La persistencia no se ha mantenido: No tienen el mismo numero de elementos");

            for (int i = 0; i < lista1.Count; i++)
            {
                Assert.IsTrue(lista1[i].Equals(lista2[i]), "El usuario en la posicion {0} no coincide",i);
            }
        }

        [Test]

        public void ComprobarPersistenciaEliminarUsuarios()
        {
            CrearValidos();
            EliminarUno(n_usuarios_iniciales);

            var lista1 = LeerUsuariosTabla();
            RefrescarPagina();
            var lista2 = LeerUsuariosTabla();

            Assert.AreEqual(lista1.Count, lista2.Count, "La persistencia no se ha mantenido: No tienen el mismo numero de elementos");

            for (int i = 0; i < lista1.Count; i++)
            {
                Assert.IsTrue(lista1[i].Equals(lista2[i]), "El usuario en la posicion {0} no coincide",i);
            }
        }


        [Test]

        public void ComprobarPersistenciaModificarUsuarios()
        {
            CrearValidos();

            ModificarUsuario(n_usuarios_iniciales, "Modificado", "33", "1.7");

            var lista1 = LeerUsuariosTabla();
            RefrescarPagina();
            var lista2 = LeerUsuariosTabla();

            Assert.AreEqual(lista1.Count, lista2.Count, "La persistencia no se ha mantenido: No tienen el mismo numero de elementos");

            for (int i = 0; i < lista1.Count; i++)
            {
                Assert.IsTrue(lista1[i].Equals(lista2[i]), "El usuario en la posicion {0} no coincide",i);
            }
        }
        
    }
}